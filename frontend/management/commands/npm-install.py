from django.core.management.base import BaseCommand
import subprocess

class Command(BaseCommand):
  help = "npm install front end dependencies from the project's root folder"

  def handle(self,*args, **kwargs):
      subprocess.run(["npm", "install", "--prefix", "frontend/"])