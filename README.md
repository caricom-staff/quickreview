# QuickReview

A website where people can quickly get their career documents reviewed by leveraging the power of online community. It's a place where you can share your resumes, cover letters, or other career documents and other users on the platform will be able to see them and provide helpful feedback.

### Design Specifications Diagram

![alt-text](design-diagram.png)

### UI Designs
To see the UI desgins, go to [this wiki](https://gitlab.com/quickreview/quickreview/wikis/UI-Designs)

### Platform Game Rules

Each user will start with a certain amount of tokens upon their signup. It will cost tokens to share a document for peer reviews and users can gain tokens by reviewing for other users' documents. The person who shared the document can rate each review they received. The higher the rating is, the more tokens the reviewer can get.

This is to create an incentive for users to review and give constructive comments on other users' documents.

Reviewers will also gain EXP (experience points) for submitting reviews. Achievements (Badges of Honor) can be earned through performing special actions such as consecutive days login or giving high-rating reviews.

Stretch Goal:

- When users have urgent documents that need to be reviewed but they ran out of tokens, they can purchase more tokens using real-world currencies. Users will be discounted for buying more tokens at once.
- If we can collect enough funding, we can allow users to exchange tokens for tangible rewards such as gift cards, discount codes to purchase products on other websites, etc.

### Tech Stack

Here is a list of technologies we are going to use in this project and the reasons why we use them:

- #### Backend
  - Django: it's a framework that has many built-in features and has security settings turned on by default. Django is also known for its scalability, which is crucial for this project since we will keep expanding the features and user base. It's also easier to learn compared to Java Spring Framework.
  - Python 3: since Python 2 will soon be deprecated in 2020.
  - PostgreSQL: Django has better support for PostgreSQL. It also has more features and it is said to be better suited for data analytics compared to MySQL.

- #### Frontend
  - React: to create functionalities that allow new items to be displayed in real-time, it's best to use a framework that uses virtual DOM. There are also more tutorials and supports for Django + React than with other front-end frameworks.
  - SASS: it makes CSS code more modular and easier to organize

### Usage Instructions

To set up the project on your local machine, you will first need to make sure you have downloaded and installed Python 3. Here is a tutorial on how to install Python 3 on different operating systems: https://realpython.com/installing-python/ Next, you can choose to follow [this tutorial](https://gitlab.com/quickreview/quickreview/wikis/Development-Tools#python-virtual-environment-optional) to install Python virtual environment to isolate your Python packages from other Python projects.

To download this repo to your local machine, open your terminal, type in `cd /folder/path/` with `/folder/path/` being the directory you want to store this project in, and then execute `git clone git@gitlab.com:quickreview/quickreview.git` or `git clone https://gitlab.com/quickreview/quickreview.git`. You should see a new folder called **quickreview** when you execute `ls` in the terminal. Type in `cd quickreview` to navigate to the project's root folder.

Before starting the server, make sure you installed the Python dependencies by running `pip install -r requirements.txt` (Note: there may be new dependencies to install in a different branch. Make sure to do the command again when you switch branches). Then, you want to create a file named `env.py` under the `QuickReview` folder (`env.py` should be in the same directory as the `settings.py`)

The content in the `env.py` should follow this format:

```
import os

# Secret key can be generated by creating a new django project and copy the SECRET_KEY string from its settings.py
os.environ['SECRET_KEY']=''

# Below are your database connection information
os.environ['DB_HOST']=''
os.environ['DB_PORT']=''
os.environ['DB_NAME']='QuickReview'
os.environ['DB_USER']=''
os.environ['DB_PASS']=''
```

The database connection values in your `env.py` should match the Postgres database server you're using. For development purposes, you can install Postgres locally on your system.
If you didn't stray from the default values during installation, you can leave the corresponding fields in `env.py` empty as Django will use the default values.

Next, you have to create a database named `QuickReview` in your Postgres database by running `psql -U postgres -c 'CREATE DATABASE "QuickReview"'` (`psql -U postgres -c "CREATE DATABASE \"QuickReview\""` in case you're on Windows).

Once you set up your database and the `env.py`, make sure to run `python manage.py makemigrations` and `python manage.py migrate` to make your database schema match the Django models.

To make the front-end displayed properly, you will need to have [Node.js](https://nodejs.org/en/download/) installed. Navigate to the `frontend` folder and run `npm install`. If all dependencies are installed successfully, here are the npm commands you can run:

- `npm run build-js`: transpile React scripts into regular Javascript
- `npm run build-css`: transpile SASS into regular CSS
- `npm run build`: execute `build-js` and `build-css` together and watch for changes
- `npm run clean`: clean up files generated by Webpack and node-sass

For more details about the npm commands, you can check out the `frontend/package.json` to see what other commands are available. For more details about the frontend commands, check out [this page](https://gitlab.com/quickreview/quickreview/wikis/Developer-Guidelines#frontend).

Once you run `npm run build`, open a new terminal, navigate to the project root folder and run `python manage.py runserver` or `./manage.py runserver` to start the server. Go to the URL the terminal prints out and you should see the homepage displays whatever changes that are made in your source code files in `frontend/src`.

### What's Next
After everything is set up, you can check out [this tutorial](https://gitlab.com/quickreview/quickreview/wikis/Development-Tools) on setting up the recommended development tools. Also, check out [this wiki](https://gitlab.com/quickreview/quickreview/wikis/home) to learn the important information for developers working on this project.
