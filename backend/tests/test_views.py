from django.test import TestCase, Client
from django.urls import reverse

class TestViews(TestCase):

    def setUp(self) -> None:
        '''
        This function will be executed before running all the tests. Best used when declaring common variables or
        when certain things need to be set up before running tests.
        '''
        self.client = Client()

    def test_home_GET(self):
        '''Send a GET request to home view and check whether the view is using the correct template'''
        response = self.client.get(reverse('home'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'home.html')

    def test_login_GET(self):
        '''Send a GET request to login view and check whether the view is using the correct template'''
        response = self.client.get(reverse('login'))
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'login.html')
