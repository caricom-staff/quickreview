from django import forms
from django.core.validators import RegexValidator

valid_pass = RegexValidator(r"([A-Za-z]+[\d@]+[\w@]*|[\d@]+[A-Za-z]+[\w@]*)", "Password needs to have both letters and numbers")

class LoginForm(forms.Form):
    email = forms.EmailField()
    password = forms.CharField(widget=forms.PasswordInput, min_length=8, validators=[valid_pass])